* **createPackage** depends on **test**
* **test** depends on **compile** and **lint**
* if **clean** is invoked with other tasks it should always be executed first
* compile and lint can potentially run in parallel 

